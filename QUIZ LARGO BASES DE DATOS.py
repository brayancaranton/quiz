from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image , ImageTk
import time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

window_width=600
window_height=1080
canvas_width =  1900
canvas_height = 980
sizex_circ=60
sizey_circ=60
width = 150
height = 150
placa = Arduino ('/dev/cu.usbmodem1421')
it = util.Iterator(placa)

#inicio el iteratodr
it.start()
veces1=0
veces2=0
veces3=0
state1=0
state2=0
state3=0
led1= placa.get_pin('d:13:o') 
led2= placa.get_pin('d:12:o') 
led3= placa.get_pin('d:11:o')

pot1= placa.get_pin('a:0:i')
pot2= placa.get_pin('a:1:i')
pot3= placa.get_pin('a:2:i')

ventana = Tk()
ventana.geometry('400x320')
ventana.configure(bg = 'white')
ventana.title("QUIZ")
draw = Canvas(ventana, width=canvas_width, height=canvas_height)
draw.place(x = 10,y = 10)


# Fetch the service account key JSON file contents
cred = credentials.Certificate('/Users/yeny/Desktop/base/secreto/llave.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://databasequiz-a7b01.firebaseio.com/'})

Label(ventana, text="SISTEMA DE CONTROL").place(x=40, y=10)
Label(ventana, text="BRAYAN CARANTON").place(x=45, y=30)
led1_draw=draw.create_oval(10,200,10+sizex_circ,200+sizey_circ,fill="white")
Label(ventana, text="LED1").place(x=27, y=275)
led2_draw=draw.create_oval(75,200,75+sizex_circ,200+sizey_circ,fill="white")
Label(ventana, text="LED2").place(x=92, y=275)
led3_draw=draw.create_oval(140,200,140+sizex_circ,200+sizey_circ,fill="white")
Label(ventana, text="LED3").place(x=157, y=275)

def medir():
    global veces1, veces2, veces3, state1, state2, state3
    ref=db.reference("adc")
    valor1=pot1.read()
    valor2=pot2.read()
    valor3=pot3.read()   
    dato1['text'] = str(valor1)
    dato2['text'] = str(valor2)
    dato3['text'] = str(valor3)
    
    ref.update({
        'adc1':{
            'estado del LED':state1,
            'valor':valor1,
            'veces encendido':veces1
            },
        'adc2':{
            'estado del LED':state2,
            'valor':valor2,
            'veces encendido':veces2
            },
        'adc3':{
            'estado del LED':state3,
            'valor':valor3,
            'veces encendido':veces3
            },
        #'alarma':mensaje1}}
            })
    
    if(valor1>=0.5 or valor2>=0.5 or valor3>=0.5):
        mensaje1['text'] = "alerta"
        mensaje1.place(x = 65,y =140) 
    else:
        mensaje1['text'] = " "
    ventana.after(500,medir)

    if(valor1>=0.5):
        led1.write(1)
        draw.itemconfig(led1_draw, fill="red")
        state1='ON'
        veces1 += 1
    else:
        led1.write(0)
        draw.itemconfig(led1_draw, fill="white")
        state1='OFF'

    if(valor2>=0.5):
        led2.write(1)
        draw.itemconfig(led2_draw, fill="yellow")
        state2='ON'
        veces2 += 1
    else:
        led2.write(0)
        draw.itemconfig(led2_draw, fill="white")
        state2='OFF'

    if(valor3>=0.5):
        led3.write(1)
        draw.itemconfig(led3_draw, fill="green")
        state3='ON'
        veces3 += 1
    else:
        led3.write(0)
        draw.itemconfig(led3_draw, fill="white")
        state3='OFF'
        
img=Image.open("/Users/yeny/Downloads/logousa.png")
img= img.resize((width,height))
imagen=ImageTk.PhotoImage(img)
b=Label(ventana,image=imagen).place(x=230,y=50)        

dato1=Label(ventana, text="0.0", bg='cadet blue2', font=("Arial Bold", 14), fg="white")
dato1.place(x=35, y=90)
Label(ventana, text="ADC1").place(x=27, y=115)
dato2=Label(ventana, text="0.0", bg='cadet blue2', font=("Arial Bold", 14), fg="white")
dato2.place(x=100, y=90)
Label(ventana, text="ADC2").place(x=92, y=115)
dato3=Label(ventana, text="0.0", bg='cadet blue2', font=("Arial Bold", 14), fg="white")
dato3.place(x=165, y=90)
Label(ventana, text="ADC3").place(x=157, y=115)
mensaje1= Label(ventana,bg="white", font=("Arial Bold", 40), fg="red")

medir()

ventana.mainloop()
